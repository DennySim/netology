FROM centos:7

RUN yum install python3 python3-pip -y
RUN pip3 install flask flask_restful

COPY python-api.py /python_api/python-api.py
CMD ["python3", "/python_api/python-api.py"]
